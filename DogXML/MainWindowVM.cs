﻿using System.ComponentModel;
using System.Windows;

namespace DogXML
{
    public class MainWindowVM : INotifyPropertyChanged
    {
        #region VM properties
        private Dogs dogs;
        public Dogs Dogs
        {
            get { return dogs; }

            set
            {
                dogs = value;
                OnPropertyChanged("Dogs");
            }
        }

        private Dog selectedDog;
        public Dog SelectedDog
        {
            get { return selectedDog; }

            set
            {
                selectedDog = value;
                OnPropertyChanged("SelectedDog");
            }
        }

        private bool editMode;
        public bool EditMode
        {
            get { return editMode; }

            set
            {
                editMode = value;
                OnPropertyChanged("EditMode");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        private XMLParser parser;

        public MainWindowVM()
        {
            parser = new XMLParser();
            EditMode = false;
            GetDogList();
        }

        public void UpdateDogList()
        {
            var isUpdateSucessful = parser.SaveDogXML(dogs);
            if (isUpdateSucessful)
            {
                MessageBox.Show("Dog list has been updated succesfully.", "Message");
                GetDogList();
                EditMode = false;

                return;
            }

            MessageBox.Show("Dog list was not able to be updated.", "Error");
        }

        private void GetDogList()
        {
            dogs = parser.ReadDogsXML();
        }        
    }
}
