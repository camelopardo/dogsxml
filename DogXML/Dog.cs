﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DogXML
{
    // THIS ATTRIBUTE IS IMPORTANT TO ADD, SO THE READER KNOWS WHERE IT SHOULD START TO READ. 
    [XmlRoot("dogs")]
    public class Dogs
    {
        // THIS ATTRIBUTE IS SUPER IMPORTANT TO ADD, OR ELSE THE XML READER WON'T KNOW WHERE TO SAVE A "DOG" ELEMENT - 1 HOUR
        [XmlElement("dog")]   
        public List<Dog> dogs { get; set; }
    }

    public class Dog
    {
        [XmlElement(ElementName = "id")]
        public int id { get; set; }

        [XmlElement(ElementName = "name")]
        public string name { get; set; }

        [XmlElement(ElementName = "age")]
        public string age { get; set; }

        [XmlElement(ElementName = "species")]
        public string species { get; set; }

        [XmlElement(ElementName = "toyDescription")]
        public string toyDescription { get; set; }

        public Dog() { }
    }
}
