﻿using System.Windows;

namespace DogXML
{
    public partial class MainWindow : Window
    {
        private MainWindowVM vm;

        public MainWindow()
        {
            InitializeComponent();
            vm = (MainWindowVM)gridMain.DataContext;
        }

        private void btnEditDog_Click(object sender, RoutedEventArgs e)
        {
            vm.EditMode = !vm.EditMode;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            vm.UpdateDogList();
        }
    }
}
