﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DogXML
{
    public class XMLParser
    {
        private string XMLPath;
        public XMLParser()
        {
            XMLPath = @"../../XMLs\DogList.xml";
        }

        // Problem: We need to save our dog list.
        // Solution: using the XMLSerializer we'll be able to serialize objects into an XML file. 
        public bool SaveDogXML(Dogs Dogs)
        {
            // I only use var if the type is obvious. Like in this case. It's obvious it's a bool.
            var success = true;

            try
            { 
                var writer = new XmlSerializer(typeof(Dogs));
                // this File.Create method creates or writes to a file on the specified path. 
                var file = File.Create(XMLPath); 
                writer.Serialize(file, Dogs);
                file.Close();
            }
            catch (Exception ex)
            {
                success = false;
                Debug.Print(ex.ToString());
            }

            return success;
        }

        // Problem: we need to convert the XML into an object.
        // Solution: With the use of the XMLSerializer and an XMLReader, 
        // we can easily convert the xml file into the object we desire.         
        public Dogs ReadDogsXML()
        {
            var dogs = new Dogs();

            try
            {
                var serializer = new XmlSerializer(typeof(Dogs));
                // The XML reader is a handy tool that allows us to quickly access XML data. 
                // It only goes foward tho', it doesn't look back at the past. It has WISDOM.           
                using (XmlReader reader = XmlReader.Create(XMLPath))
                {
                    dogs = (Dogs)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
            }

            return dogs;
        }
    }
}
